package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/product_service/genproto/product_service"
	"gitlab.com/market_go/product_service/pkg/helper"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *CategoryRepo {
	return &CategoryRepo{
		db: db,
	}
}

func (r *CategoryRepo) Create(ctx context.Context, req *product_service.CategoryCreate) (*product_service.Category, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO categories(id, name, parent_id, brand, updated_at)
		VALUES ($1, $2, $3, $4, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		helper.NewNullString(req.ParentId),
		helper.NewNullString(req.Brand),
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Category{
		Id:       id,
		Name:     req.Name,
		ParentId: req.ParentId,
		Brand:    req.Brand,
	}, nil
}

func (r *CategoryRepo) GetByID(ctx context.Context, req *product_service.CategoryPrimaryKey) (*product_service.Category, error) {

	var (
		query string

		Id        sql.NullString
		Name      sql.NullString
		ParentId  sql.NullString
		Brand     sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			parent_id,
			brand,
			created_at,
			updated_at
		FROM categories
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&Name,
		&ParentId,
		&Brand,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Category{
		Id:        Id.String,
		Name:      Name.String,
		ParentId:  ParentId.String,
		Brand:     Brand.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *CategoryRepo) GetList(ctx context.Context, req *product_service.CategoryGetListRequest) (*product_service.CategoryGetListResponse, error) {

	var (
		resp   = &product_service.CategoryGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			parent_id,
			brand,
			created_at,
			updated_at	
		FROM categories
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			Name      sql.NullString
			ParentId  sql.NullString
			Brand     sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Name,
			&ParentId,
			&Brand,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Categories = append(resp.Categories, &product_service.Category{
			Id:        Id.String,
			Name:      Name.String,
			ParentId:  ParentId.String,
			Brand:     Brand.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *CategoryRepo) Update(ctx context.Context, req *product_service.CategoryUpdate) (*product_service.Category, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			categories
		SET
			name = :name,
			parent_id = :parent_id,
			brand = :brand,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.Id,
		"name":      req.Name,
		"parent_id": helper.NewNullString(req.ParentId),
		"brand":     helper.NewNullString(req.Brand),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &product_service.Category{
		Id:       req.Id,
		Name:     req.Name,
		ParentId: req.ParentId,
		Brand:    req.Brand,
	}, nil
}

func (r *CategoryRepo) Delete(ctx context.Context, req *product_service.CategoryPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM categories WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
