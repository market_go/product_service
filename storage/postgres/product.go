package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/product_service/genproto/product_service"
	"gitlab.com/market_go/product_service/pkg/helper"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *ProductRepo {
	return &ProductRepo{
		db: db,
	}
}

func (r *ProductRepo) Create(ctx context.Context, req *product_service.ProductCreate) (*product_service.Product, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO products(id, photo, name, category, brand, barcode, price, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Photo,
		req.Name,
		helper.NewNullString(req.Category),
		helper.NewNullString(req.Brand),
		req.Barcode,
		req.Price,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Product{
		Id:       id,
		Photo:    req.Photo,
		Name:     req.Name,
		Category: req.Category,
		Brand:    req.Brand,
		Barcode:  req.Barcode,
		Price:    req.Price,
	}, nil
}

func (r *ProductRepo) GetByID(ctx context.Context, req *product_service.ProductPrimaryKey) (*product_service.Product, error) {

	var (
		query string

		Id        sql.NullString
		Name      sql.NullString
		Photo     sql.NullString
		Category  sql.NullString
		Brand     sql.NullString
		Barcode   sql.NullString
		Price     sql.NullInt64
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			photo,
			category,
			brand,
			barcode,
			price,
			created_at,
			updated_at
		FROM products
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&Name,
		&Photo,
		&Category,
		&Brand,
		&Barcode,
		&Price,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Product{
		Id:        Id.String,
		Name:      Name.String,
		Photo:     Photo.String,
		Category:  Category.String,
		Brand:     Brand.String,
		Barcode:   Barcode.String,
		Price:     Price.Int64,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *ProductRepo) GetList(ctx context.Context, req *product_service.ProductGetListRequest) (*product_service.ProductGetListResponse, error) {

	var (
		resp   = &product_service.ProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			photo,
			category,
			brand,
			barcode,
			price,
			created_at,
			updated_at	
		FROM products
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	if req.SearchByBarcode != "" {
		where += ` AND barcode ILIKE '` + req.SearchByBarcode + `'`
	}

	query += where + offset + limit
	fmt.Println(query)
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			Name      sql.NullString
			Photo     sql.NullString
			Category  sql.NullString
			Brand     sql.NullString
			Barcode   sql.NullString
			Price     sql.NullInt64
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Name,
			&Photo,
			&Category,
			&Brand,
			&Barcode,
			&Price,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Products = append(resp.Products, &product_service.Product{
			Id:        Id.String,
			Name:      Name.String,
			Photo:     Photo.String,
			Category:  Category.String,
			Brand:     Brand.String,
			Barcode:   Barcode.String,
			Price:     Price.Int64,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ProductRepo) Update(ctx context.Context, req *product_service.ProductUpdate) (*product_service.Product, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			products
		SET
			photo = :photo,
			name = :name,
			category = :category,
			brand = :brand,
			barcode = :barcode,
			price = :price,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":       req.Id,
		"photo":    req.Photo,
		"name":     req.Name,
		"category": helper.NewNullString(req.Category),
		"brand":    helper.NewNullString(req.Brand),
		"barcode":  req.Barcode,
		"price":    req.Price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &product_service.Product{
		Id:       req.Id,
		Name:     req.Name,
		Photo:    req.Photo,
		Category: req.Category,
		Brand:    req.Brand,
		Barcode:  req.Barcode,
		Price:    req.Price,
	}, nil
}

func (r *ProductRepo) Delete(ctx context.Context, req *product_service.ProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM products WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
