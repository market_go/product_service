package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/product_service/genproto/product_service"
	"gitlab.com/market_go/product_service/pkg/helper"
)

type BrandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) *BrandRepo {
	return &BrandRepo{
		db: db,
	}
}

func (r *BrandRepo) Create(ctx context.Context, req *product_service.BrandCreate) (*product_service.Brand, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO brands(id, name, photo, updated_at)
		VALUES ($1, $2, $3, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Photo,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Brand{
		Id:    id,
		Name:  req.Name,
		Photo: req.Photo,
	}, nil
}

func (r *BrandRepo) GetByID(ctx context.Context, req *product_service.BrandPrimaryKey) (*product_service.Brand, error) {

	var (
		query string

		Id        sql.NullString
		Name      sql.NullString
		Photo     sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			photo,
			created_at,
			updated_at
		FROM brands
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&Name,
		&Photo,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Brand{
		Id:        Id.String,
		Name:      Name.String,
		Photo:     Photo.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *BrandRepo) GetList(ctx context.Context, req *product_service.BrandGetListRequest) (*product_service.BrandGetListResponse, error) {

	var (
		resp   = &product_service.BrandGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			photo,
			created_at,
			updated_at	
		FROM brands
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			Name      sql.NullString
			Photo     sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Name,
			&Photo,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Brandes = append(resp.Brandes, &product_service.Brand{
			Id:        Id.String,
			Name:      Name.String,
			Photo:     Photo.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *BrandRepo) Update(ctx context.Context, req *product_service.BrandUpdate) (*product_service.Brand, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			brands
		SET
			name = :name,
			photo = :photo,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":    req.Id,
		"name":  helper.NewNullString(req.Name),
		"photo": helper.NewNullString(req.Photo),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &product_service.Brand{
		Id:    req.Id,
		Name:  req.Name,
		Photo: req.Photo,
	}, nil
}

func (r *BrandRepo) Delete(ctx context.Context, req *product_service.BrandPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM brands WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
