--Product Service 
CREATE TABLE brands(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR,
    "photo" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE categories(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR,
    "parent_id" UUID REFERENCES categories("id"),
    "brand" UUID REFERENCES brands("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE products(
    "id" UUID PRIMARY KEY,
    "photo" VARCHAR,
    "name" VARCHAR,
    "category" UUID REFERENCES categories("id"),
    "brand" UUID REFERENCES brands("id"),
    "barcode" VARCHAR UNIQUE,
    "price" INT NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);